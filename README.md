# Dante

Epitech project: Perfect and imperfect maze generator and triple resolution algorithms.

## Getting Started

### Prerequisites

You need **gcc**, **make** & the **C library**.

### Installing

```
cd dante
make
```

## Running the tests

To generate a maze

```
./generateur x y [perfect | imperfect]
```

To solve a maze

```
./astar/solver [Maze]
./largeur/solver [Maze]
./profondeur/solver [Maze]
```

## Authors

* **Vincent LE QUEC** - [Venatum](https://gitlab.com/vincent.le-quec)