/*
** my_str_to_wordtab.h for my_str_to_wordtab in /home/pellet_r/test
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Sun Jan 24 20:35:00 2016 remy PELLETIER
** Last update Sun Jan 24 20:36:49 2016 remy PELLETIER
*/

#ifndef _MY_STR_TO_WORDTAB_H
#define _MY_STR_TO_WORDTAB_H

typedef struct	s_data
{
  int	i;
  int	j;
  int	k;
  int	b;
}		t_data;

#endif
