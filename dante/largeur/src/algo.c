/*
** algo.c for algo in /home/pellet_r/CPE/dante/tmp_largeur/old
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Tue May 17 12:04:32 2016 remy PELLETIER
** Last update Sat May 21 14:25:31 2016 Vincent Le Quec
*/

#include "labyrinth.h"

void            find_possibilite(t_labyrinth *lab, int pos, int *tab_possib)
{
  int           i;
  int           j;
  int           k;

  k = 0;
  while (tab_possib[k] > 0)
    {
      lab->pos[lab->index_en_cour] = what_position(lab, pos, tab_possib[k]);
      i = lab->pos[lab->index_en_cour] / lab->width;
      j = lab->pos[lab->index_en_cour] % lab->width;
      if (lab->index_parent > 0)
        lab->pos_parent[i][j] = lab->pos[lab->index_parent - 1];
      else
        lab->pos_parent[i][j] = 0;
      lab->tab[i][j] = 'A';
      k++;
      lab->index_en_cour++;
    }
}

int             algo(t_labyrinth *lab, int pos)
{
  int           *tab_possib;

  while (lab->i != lab->end_i || lab->j != lab->end_j)
    {
      if (lab->index_en_cour != 0)
        {
          if ((lab->pos = realloc(lab->pos, sizeof(*lab->pos) *
                                  (lab->index_en_cour + 10))) == NULL)
	    my_perror("realloc");
        }
      tab_possib = what_num(lab, lab->i, lab->j);
      if (tab_possib != NULL)
	  find_possibilite(lab, pos, tab_possib);
      lab->tab[lab->i][lab->j] = '#';
      pos = lab->pos[lab->index_parent];
      lab->i = lab->pos[lab->index_parent] / lab->width;
      lab->j = lab->pos[lab->index_parent] % lab->width;
      free(tab_possib);
      if (lab->index_parent == lab->index_en_cour)
	return (-1);
      lab->index_parent++;
    }
  lab->tab[lab->i][lab->j] = '#';
  display_path(lab);
  return (0);
}

int             check_begin(t_labyrinth *lab)
{
  if (lab->tab[0] == NULL)
    {
      fprintf(stderr, "File Null\n");
      return (-1);
    }
  if (lab->tab[0][0] != '*')
    {
      fprintf(stderr, "I can't start your maze\n");
      return (-1);
    }
  if (lab->tab[lab->end_i][lab->end_j] != '*')
    {
      fprintf(stderr, "I can't finish your maze\n");
      return (-1);
    }
  return (0);
}

int             largeur(t_labyrinth *lab, char **av)
{
  create_tab(lab);
  create_tab_parent(lab);
  lab->pos_parent[0][0] = -1;
  fill_tab(lab, av);
  if ((check_begin(lab)) == -1)
    return (-2);
  if ((algo(lab, 0)) == -1)
    {
      fprintf(stderr, "%s\n", "Your maze is impossible");
      return (-2);
    }
  return (0);
}
