/*
** check_position.c for check_position in /home/pellet_r/CPE/dante/tmp_largeur/old
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Tue May 17 11:52:00 2016 remy PELLETIER
** Last update Wed May 18 12:23:43 2016 remy PELLETIER
*/

#include "labyrinth.h"

int             check_left(t_labyrinth *lab, int i, int j)
{
  if (j == 0)
    return (0);
  else
    if (lab->tab[i][j - 1] == '*')
      return (4);
  return (0);
}

int             check_right(t_labyrinth *lab, int i, int j)
{
  if (j == lab->width - 1)
    return (0);
  else
    if (lab->tab[i][j + 1] == '*')
      return (2);
  return (0);
}

int             check_top(t_labyrinth *lab, int i, int j)
{
  if (i == 0)
    return (0);
  else
    if (lab->tab[i - 1][j] == '*')
      return (1);
  return (0);
}

int             check_bottom(t_labyrinth *lab, int i, int j)
{
  if (i == lab->height - 1)
    return (0);
  else
    if (lab->tab[i + 1][j] == '*')
      return (3);
  return (0);
}

int             what_position(t_labyrinth *lab, int pos, int nb_alea)
{
  if (nb_alea == 1)
    return (pos - (lab->width * 1));
  if (nb_alea == 2)
    return (pos + 1);
  if (nb_alea == 3)
    return (pos + (lab->width * 1));
  if (nb_alea == 4)
    return (pos - 1);
  return (0);
}
