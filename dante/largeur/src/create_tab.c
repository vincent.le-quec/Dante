/*
** create_tab.c for create_tab in /home/pellet_r/CPE/dante/tmp_largeur/old
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Tue May 17 11:58:22 2016 remy PELLETIER
** Last update Wed May 18 12:59:21 2016 Vincent Le Quec
*/

#include "labyrinth.h"

int             fill_tab(t_labyrinth *lab, char **av)
{
  FILE*         fd;
  char          *buff;
  size_t        size;
  int           i;
  int           j;

  i = 0;
  buff = NULL;
  if ((fd = fopen(av[1], "r+")) == NULL)
    my_perror("fopen");
  while ((getline(&buff, &size, fd)) != -1)
    {
      j = 0;
      while (buff[j] != '\n' && buff[j] != '\0')
        {
          lab->tab[i][j] = buff[j];
          j++;
        }
      lab->tab[i][j] = '\0';
      i++;
    }
  lab->tab[i] = NULL;
  free(buff);
  fclose(fd);
  return (0);
}

int             create_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  if ((lab->tab = malloc(sizeof(char *) * lab->height + SECURE)) == NULL)
    my_perror("malloc");
  while (i < lab->height)
    {
      if ((lab->tab[i] = malloc(sizeof(char) * lab->width)) == NULL)
	my_perror("malloc");
      i++;
    }
  lab->tab[i] = NULL;
  return (0);
}

int             create_tab_parent(t_labyrinth *lab)
{
  int           i;

  i = 0;
  if ((lab->pos_parent = malloc(sizeof(*lab->pos_parent) *
				lab->height + SECURE)) == NULL)
    my_perror("malloc");
  while (i < lab->height)
    {
      if ((lab->pos_parent[i] = malloc(sizeof(**lab->pos_parent) *
				       lab->width)) == NULL)
	my_perror("malloc");
      i++;
    }
  lab->pos_parent[i] = NULL;
  return (0);
}
