/*
** main.c for main in 
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Mon Apr 25 10:53:09 2016 remy PELLETIER
** Last update Sat May 21 14:34:57 2016 Vincent Le Quec
*/

#include "labyrinth.h"

int		check_error_main(t_labyrinth *lab, char **av)
{
  int		check;

  check = init_struct(lab, av);
  if (check == -1)
    {
      free(lab);
      fprintf(stderr, "Bad file : probleme expected line [%d]\n",
	      lab->height + 1);
      return (-1);
    }
  if (check == -3)
    {
      free(lab);
      return (-1);
    }
  return (0);
}

int		main(int ac, char **av)
{
  t_labyrinth	*lab;

  if (ac == 2)
    {
      lab = NULL;
      if ((lab = malloc(sizeof(*lab))) == NULL)
	my_perror("malloc");
      if ((check_error_main(lab, av)) != 0)
	return (-1);
      if ((largeur(lab, av)) != -2)
	{
	  epur_tab(lab);
	  display_tab(lab);
	}
      free(lab->pos);
      free_tab_int(lab);
      free_tab_char(lab);
      free(lab);
    }
  else
    printf("Usage: ./solver [maze]\n");
  return (0);
}
