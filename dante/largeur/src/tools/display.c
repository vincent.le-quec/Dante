/*
** display.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Thu May 12 17:44:28 2016 Vincent Le Quec
** Last update Wed May 18 12:24:01 2016 remy PELLETIER
*/

#include "labyrinth.h"

void            display_tab_color(char **tab)
{
  int           i;
  int           j;

  i = 0;
  while (tab[i])
    {
      j = 0;
      while (tab[i][j])
        {
          if (tab[i][j] == 'o')
            printf(BOLDGREEN "o" RESET);
          else
            if (tab[i][j] == 'X')
              printf(MAGENTA "X" RESET);
	    else
	      printf(BOLDBLUE "*" RESET);
          j++;
        }
      printf("\n");
      i++;
    }
}

void            display_path(t_labyrinth *lab)
{
  int           i;
  int           j;

  while (lab->pos_parent[lab->i][lab->j] != -1)
    {
      lab->tab[lab->i][lab->j] = 'Q';
      i = lab->i;
      j = lab->j;
      lab->i = lab->pos_parent[i][j] / lab->width;
      lab->j = lab->pos_parent[i][j] % lab->width;
    }
  lab->tab[lab->i][lab->j] = 'Q';
}
