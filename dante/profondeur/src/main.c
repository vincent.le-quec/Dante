/*
** main.c for  in /home/le-que_v/rendu/dante/profondeur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May 16 12:52:32 2016 Vincent Le Quec
** Last update Sat May 21 14:55:59 2016 Vincent Le Quec
*/

#include "profondeur.h"

int             check_error_main(t_labyrinth *lab, char **av)
{
  int           check;

  check = init_struct(lab, av);
  if (check == -1)
    {
      free(lab);
      fprintf(stderr, "Bad file : probleme expected line %d\n",
	      lab->height + 1);
      return (-1);
    }
  if (check == -3)
    {
      free(lab);
      return (-1);
    }
  return (0);
}

int		main(int ac, char **av)
{
  t_labyrinth	*lab;

  if (ac == 2)
    {
      lab = NULL;
      if ((lab = malloc(sizeof(*lab))) == NULL)
	my_perror("malloc");
      if ((check_error_main(lab, av)) != 0)
        return (-1);
      if ((labyrinth(lab, av)) != 0)
	return (-1);
      epur_tab(lab);
      display_tab(lab);
      free_tab(lab);
      free(lab);
    }
  else
    printf("Usage: ./solver maze\n");
  return (0);
}
