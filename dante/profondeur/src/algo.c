/*
** algo.c for algo in /home/pellet_r/CPE/dante/profondeur
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Fri May 20 15:42:37 2016 remy PELLETIER
** Last update Sat May 21 14:55:39 2016 Vincent Le Quec
*/

#include "profondeur.h"

int             what_position(t_labyrinth *lab, int pos, int nb_alea)
{
  if (nb_alea == 1)
    return (pos - lab->width);
  if (nb_alea == 2)
    return (pos + 1);
  if (nb_alea == 3)
    return (pos + lab->width);
  if (nb_alea == 4)
    return (pos - 1);
  return (0);
}

int             algo2(t_labyrinth *lab, int pos, int nb_alea)
{
  nb_alea = 0;
  if ((nb_alea = what_num(lab, lab->i, lab->j, '*')) != 0)
    {
      lab->tab[lab->i][lab->j] = 'o';
      pos = what_position(lab, pos, nb_alea);
    }
  else
    {
      lab->tab[lab->i][lab->j] = 'A';
      nb_alea = what_num(lab, lab->i, lab->j, 'o');
      pos = what_position(lab, pos, nb_alea);
    }
  return (pos);
}

int             algo(t_labyrinth *lab)
{
  int           nb_alea;
  int           pos;
  int           check;

  check = 0;
  pos = 0;
  nb_alea = 0;
  while (lab->j != lab->end_j || lab->i != lab->end_i)
    {
      lab->i = pos / (lab->width);
      lab->j = pos % (lab->width);
      pos = algo2(lab, pos, nb_alea);
      if (lab->j == 0 && lab->i == 0 && check != 0)
        return (-1);
      check++;
    }
  lab->tab[lab->i][lab->j] = 'o';
  return (0);
}

int             labyrinth(t_labyrinth *lab, char **av)
{
  if ((create_tab(lab)) == -1 || (fill_tab(lab, av)) == -1)
    return (-1);
  if ((check_begin(lab)) != 0)
    return (-2);
  if ((algo(lab)) == -1)
    {
      fprintf(stderr, "%s\n", "Your maze is impossible");
      return (-2);
    }
  return (0);
}
