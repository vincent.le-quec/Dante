/*
** init_struc.c for init_struct in /home/pellet_r/CPE/dante/profondeur
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Fri May 20 15:45:37 2016 remy PELLETIER
** Last update Sat May 21 14:55:52 2016 Vincent Le Quec
*/

#include "profondeur.h"

char            find_bad_char(char *buff)
{
  int           i;

  i = 0;
  while (buff[i])
    {
      if (buff[i] != '*' && buff[i] != 'X' && buff[i] != '\n')
        return (buff[i]);
      i++;
    }
  return ('*');
}

int             calc_width_height2(char         *buff,
                                   FILE         *fd,
                                   t_labyrinth  *lab,
                                   size_t       size)
{
  int           width_check;
  char          c;

  width_check = 0;
  while ((getline(&buff, &size, fd)) != -1)
    {
      if ((c = find_bad_char(buff)) != '*')
        {
          free(buff);
          fprintf(stderr, "Bad charactere expected [%c]\n", c);
          return (-3);
        }
      lab->width = strlen(buff);
      if (width_check == 0)
        width_check = lab->width;
      if (width_check != lab->width)
        {
          free(buff);
          return (-1);
        }
      lab->height += 1;
    }
  free(buff);
  return (0);
}

int             calc_width_height(t_labyrinth *lab, char **av)
{
  FILE          *fd;
  char          *buff;
  int           check;
  size_t        size;

  size = 0;
  fd = NULL;
  buff = NULL;
  if ((fd = fopen(av[1], "r+")) == NULL)
    my_perror("fopen");
  check = calc_width_height2(buff, fd, lab, size);
  fclose(fd);
  return (check);
}

int             init_struct(t_labyrinth *lab, char **av)
{
  int           check;

  lab->tab = NULL;
  lab->height = 1;
  lab->width = 0;
  if ((check = calc_width_height(lab, av)) != 0)
    return (check);
  lab->end_i = lab->height - 2;
  lab->end_j = lab->width - 2;
  lab->i = 0;
  lab->j = 0;
  if (lab->width <= 0 || lab->height <= 0)
    return (-1);
  return (0);
}
