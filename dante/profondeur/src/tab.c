/*
** tab.c for  in /home/le-que_v/rendu/dante/profondeur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May 16 13:12:16 2016 Vincent Le Quec
** Last update Sat May 21 14:56:08 2016 Vincent Le Quec
*/

#include "profondeur.h"

int             check_begin(t_labyrinth *lab)
{
  if (lab->tab[0] == NULL)
    {
      fprintf(stderr, "File Null\n");
      return (-1);
    }
  if (lab->tab[0][0] != '*')
    {
      fprintf(stderr, "I can't start your maze\n");
      return (-1);
    }
  if (lab->tab[lab->end_i][lab->end_j] != '*')
    {
      fprintf(stderr, "I can't finish your maze\n");
      return (-1);
    }
  return (0);
}

int             create_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  if ((lab->tab = malloc(sizeof(char *) * lab->height)) == NULL)
    my_perror("malloc");
  while (i < lab->height)
    {
      if ((lab->tab[i] = malloc(sizeof(char) * lab->width)) == NULL)
	my_perror("malloc");
      i++;
    }
  return (0);
}

int             fill_tab(t_labyrinth *lab, char **av)
{
  FILE*         fd;
  char          *buff;
  size_t        size;
  int           i;
  int           j;

  i = 0;
  buff = NULL;
  if ((fd = fopen(av[1], "r+")) == NULL)
    my_perror("fopen");
  while ((getline(&buff, &size, fd)) != -1)
    {
      j = 0;
      while (buff[j] != '\n' && buff[j] != '\0')
        {
          lab->tab[i][j] = buff[j];
          j++;
        }
      lab->tab[i][j] = '\0';
      i++;
    }
  lab->tab[i] = NULL;
  free(buff);
  fclose(fd);
  return (0);
}
