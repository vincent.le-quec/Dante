/*
** tools.c for  in /home/le-que_v/rendu/dante/profondeur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May 16 12:54:02 2016 Vincent Le Quec
** Last update Sat May 21 14:56:24 2016 Vincent Le Quec
*/

#include "profondeur.h"

void            display_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  while (lab->tab[i])
    printf("%s\n", lab->tab[i++]);
}

void            free_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  while (lab->tab[i])
    free(lab->tab[i++]);
  free(lab->tab);
}

void            epur_tab(t_labyrinth *lab)
{
  int           i;
  int           j;

  j = 0;
  i = 0;
  while (lab->tab[i])
    {
      j = 0;
      while (lab->tab[i][j])
        {
          if (lab->tab[i][j] == 'A')
            lab->tab[i][j] = '*';
          j++;
        }
      i++;
    }
}
