/*
** error.c for  in /home/le-que_v/rendu/dante/profondeur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May 16 13:19:45 2016 Vincent Le Quec
** Last update Sat May 21 14:56:16 2016 Vincent Le Quec
*/

#include "profondeur.h"

void	my_perror(char *str)
{
  perror(str);
  exit(1);
}
