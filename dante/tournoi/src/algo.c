/*
** algo.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon Apr 25 18:01:40 2016 Vincent Le Quec
** Last update Fri May 20 17:01:54 2016 remy PELLETIER
*/

#include "solver.h"

int		check_min(t_astar *a, double *res)
{
  double	min;
  int		i;

  if ((min = my_min(res, 4)) == -1)
    return (1);
  i = find_case_tab(res, 4, min);
  if (i == 0)
    a->cell.x++;
  if (i == 1)
    a->cell.x--;
  if (i == 2)
    a->cell.y++;
  if (i == 3)
    a->cell.y--;
  a->tab[a->cell.y][a->cell.x] = 'o';
  return (0);
}

int	go_back(t_astar *a)
{
  while ((check_co_back(a, 'o')) != 1);
    {
      a->tab[a->cell.y][a->cell.x] = 'A';
      if (a->cell.x != a->width - 1 &&
	  a->tab[a->cell.y][a->cell.x + 1] == 'o')
	a->cell.x++;
      else
	if (a->cell.x != 0 && a->tab[a->cell.y][a->cell.x - 1] == 'o')
	a->cell.x--;
      else
	if (a->tab[a->cell.y + 1] != NULL &&
	    a->tab[a->cell.y + 1][a->cell.x] == 'o')
	  a->cell.y++;
      else
	if (a->cell.y != 0 && a->tab[a->cell.y - 1][a->cell.x] == 'o')
	  a->cell.y--;
    }
  a->tab[a->cell.y][a->cell.x] = 'o';
  return (0);
}

int	algo(t_astar *a)
{
  int	check;

  check = 0;
  while (a->cell.x != a->end.x || a->cell.y != a->end.y)
    {
      a->tab[a->cell.y][a->cell.x] = 'o';
      if ((check_co(a, '*')) == 1)
	go_back(a);
      if (a->cell.x == 0 && a->cell.y == 0 && check != 0)
	{
	  fprintf(stderr, "Your maze is impossible\n");
	  return (-1);
	}
      check++;
    }
  return (0);
}
