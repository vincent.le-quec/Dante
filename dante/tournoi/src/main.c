/*
** main.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon Apr 25 15:06:10 2016 Vincent Le Quec
** Last update Fri May 20 17:24:37 2016 remy PELLETIER
*/

#include "solver.h"

int		check_begin(t_astar *a)
{
  if (a->tab[0] == NULL)
    {
      fprintf(stderr, "File Null\n");
      return (-1);
    }
  if (a->tab[0][0] != '*')
    {
      fprintf(stderr, "I can't start your maze\n");
      return (-1);
    }
  if (a->tab[a->end.y][a->end.x] != '*')
    {
      fprintf(stderr, "I can't finish your maze\n");
      return (-1);
    }
  a->tab[a->cell.y][a->cell.x] = 'o';
  return (0);
}

void            epur_tab(t_astar *a)
{
  int           i;
  int           j;

  i = 0;
  while (a->tab[i])
    {
      j = 0;
      while (a->tab[i][j])
        {
          if (a->tab[i][j] == 'A')
            a->tab[i][j] = '*';
          j++;
        }
      i++;
    }
}

int             check_error_main(t_astar *a, char **av)
{
  int           check;

  check = ini_struct(a, av);
  if (check == -1)
    {
      free(a);
      fprintf(stderr, "Bad file : probleme expected line %d\n",
              a->height + 1);
      return (-1);
    }
  if (check == -3)
    {
      free(a);
      return (-1);
    }
  return (0);
}

int		main(int ac, char **av)
{
  t_astar	*a;

  if (ac == 2)
    {
      if ((a = malloc(sizeof(*a))) == NULL)
	my_perror("malloc");
      if ((check_error_main(a, av)) != 0)
	return (-1);
      load_file(a, av);
      if ((check_begin(a)) == 0)
	{
	  if ((algo(a)) == 0)
	    {
	      epur_tab(a);
	      display_tab_color(a->tab);
	    }
	}
      free_tab(a);
      free(a);
    }
  else
    printf("Usage: ./solver [Maze]\n");
  return (0);
}
