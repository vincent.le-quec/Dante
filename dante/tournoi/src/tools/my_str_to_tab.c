/*
** my_str_to_tab.c for  in /home/le-que_v/rendu/PSU_2015_minishell1
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon Jan 11 14:01:22 2016 Vincent Le Quec
** Last update Thu Apr 28 10:27:43 2016 Vincent Le Quec
*/

#include "solver.h"

int	my_n(char *str, char sep)
{
  int	nb;
  int	i;

  i = 0;
  nb = 0;
  while (str[i])
    {
      if (str[i] == sep)
	nb++;
      i++;
    }
  return (nb);
}

int	no_space_or_tab(int i, char *str, char sep, int z)
{
  if (z == 0)
    while (str[i] == sep)
      i++;
  if (z == 1)
    while (str[i + 1] == sep)
      i++;
  return (i);
}

char	**my_remp(char **tab, char *str, char sep, int i)
{
  int	j;
  int	k;

  j = 0;
  k = 0;
  if ((tab[j] = malloc(sizeof(char) * (strlen(str) + 1))) == NULL)
    return (NULL);
  i = no_space_or_tab(i, str, sep, 0);
  while (str[i])
    {
      if (str[i] == sep)
	{
	  i = no_space_or_tab(i, str, sep, 1);
	  tab[j][k] = '\0';
	  j++;
	  k = -1;
	  if (str[i + 1] == '\0')
	    return (tab);
	  if ((tab[j] = malloc(sizeof(char) * (strlen(str) + 1))) == NULL)
	    return (NULL);
	}
      tab[j][k++] = str[i++];
    }
  tab[++j] = NULL;
  return (tab);
}

char	**my_str_to_tab(char *str, char sep)
{
  char	**tab;
  int	i;

  i = 0;
  if ((tab = malloc(sizeof(char*) * (my_n(str, sep) + 1))) == NULL)
    return (NULL);
  return (my_remp(tab, str, sep, i));
}
