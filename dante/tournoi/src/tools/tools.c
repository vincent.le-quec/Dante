/*
** tools.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon Apr 25 17:56:25 2016 Vincent Le Quec
** Last update Fri May 20 17:09:23 2016 remy PELLETIER
*/

#include "solver.h"

double          my_min(double *tab, int len)
{
  double        tmp;
  int           i;

  i = 0;
  tmp = tab[i];
  while (i < len)
    {
      if (tab[i] >= 0)
        tmp = tab[i];
      i++;
    }
  i = 0;
  while (i < len)
    {
      if ((tab[i] < tmp) && (tab[i] >= 0))
        tmp = tab[i];
      i++;
    }
  return (tmp);
}

int             find_case_tab(double *res, int size, double nb)
{
  int           i;

  i = 0;
  while (i < size)
    {
      if (res[i] == nb)
        return (i);
      i++;
    }
  return (-1);
}

void		free_tab(t_astar *a)
{
  int		i;

  i = 0;
  while (a->tab[i])
    free(a->tab[i++]);
  free(a->tab);
}

int		ini_struct(t_astar *a, char **av)
{
  int		check;

  a->tab = NULL;
  a->height = 0;
  a->width = 0;
  if ((check = calc_width_height(a, av)) != 0)
    return (check);
  a->cell.x = 0;
  a->cell.y = 0;
  a->tmp.x = 0;
  a->tmp.y = 0;
  a->end.x = a->width - 2;
  a->end.y = a->height - 1;
  a->exit = 0;
  return (0);
}
