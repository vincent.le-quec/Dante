/*
** solver.h for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon Apr 25 10:59:11 2016 Vincent Le Quec
** Last update Fri May 20 17:08:42 2016 remy PELLETIER
*/

#ifndef		_SOLVER_H_
# define	_SOLVER_H_

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include "color.h"
#include "my.h"

#define ABS(x) (x > 0 ? x : -x)
#define	STOP		42
#define	PRECISION	4

typedef struct	s_pos
{
  int		x;
  int		y;
}		t_pos;

typedef struct	s_astar
{
  char		**tab;
  t_pos		cell;
  t_pos		tmp;
  t_pos		end;
  int		width;
  int		height;
  int		exit;
}		t_astar;

ssize_t		getline(char **lineptr, size_t *n, FILE *stream);

/* Algo */
int		algo(t_astar *a);
int             check_co(t_astar *a, char c);
int             check_min(t_astar *a, double *res);
double          distance(t_astar *a, int x, int y);
int		check_co_back(t_astar *a, char c);

/*check_co*/
double		check_left(t_astar *a, char c);
double		check_top(t_astar *a, char c);
double		check_bottom(t_astar *a, char c);
double		check_right(t_astar *a, char c);

/* Display */
void		display_tab(char **tab);
void		display_tab_color(char **tab);

/* Tools */
void            my_perror(char *str);
char		**my_str_to_tab(char *str, char sep);
int		ini_struct(t_astar *a, char **av);
double          my_min(double *tab, int len);
int             find_case_tab(double *res, int size, double nb);
int             calc_width_height(t_astar *a, char **av);
void            load_file(t_astar *a, char **av);
void            free_struct(t_astar *a);
void		free_tab(t_astar *a);

#endif
