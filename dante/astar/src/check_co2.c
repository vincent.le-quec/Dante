/*
** check_co2.c for check_co2 in /home/pellet_r/CPE/dante/astar/tmp_astar
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Fri May 20 17:02:58 2016 remy PELLETIER
** Last update Fri May 20 17:03:43 2016 remy PELLETIER
*/

#include "solver.h"

double          check_right(t_astar *a, char c)
{
  double        res;

  res = 0.0;
  if (a->cell.x != a->width - 1)
    (a->tab[a->cell.y][a->cell.x + 1] == c)
      ? res = distance(a, a->cell.x + 1, a->cell.y) : (res = -1.0);
  else
    res = -1.0;
  return (res);
}

double          check_left(t_astar *a, char c)
{
  double        res;

  res = 0.0;
  if (a->cell.x != 0)
    (a->tab[a->cell.y][a->cell.x - 1] == c)
      ? res = distance(a, a->cell.x - 1, a->cell.y) : (res = -1.0);
  else
    res = -1.0;
  return (res);
}

double          check_bottom(t_astar *a, char c)
{
  double        res;

  res = 0.0;
  if (a->tab[a->cell.y + 1] != NULL)
    (a->tab[a->cell.y + 1][a->cell.x] == c)
      ? res = distance(a, a->cell.x, a->cell.y + 1) : (res = -1.0);
  else
    res = -1.0;
  return (res);
}

double          check_top(t_astar *a, char c)
{
  double        res;

  res = 0.0;
  if (a->cell.y != 0)
    (a->tab[a->cell.y - 1][a->cell.x] == c)
      ? res = distance(a, a->cell.x, a->cell.y - 1) : (res = -1.0);
  else
    res = -1.0;
  return (res);
}

