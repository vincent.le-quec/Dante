/*
** create_tab.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Thu Apr 28 10:33:36 2016 Vincent Le Quec
** Last update Fri May 20 16:48:40 2016 remy PELLETIER
*/

#include "solver.h"

char            find_bad_char(char *buff)
{
  int           i;

  i = 0;
  while (buff[i])
    {
      if (buff[i] != '*' && buff[i] != 'X' && buff[i] != '\n')
        return (buff[i]);
      i++;
    }
  return ('*');
}

int             calc_width_height2(char         *buff,
                                   FILE         *fd,
                                   t_astar	*a,
                                   size_t       size)
{
  int           width_check;
  char          c;

  width_check = 0;
  while ((getline(&buff, &size, fd)) != -1)
    {
      if ((c = find_bad_char(buff)) != '*')
        {
          free(buff);
          fprintf(stderr, "Bad charactere expected [%c]\n", c);
          return (-3);
        }
      a->width = strlen(buff);
      if (width_check == 0)
        width_check = a->width;
      if (width_check != a->width)
        {
          free(buff);
          return (-1);
        }
      a->height += 1;
    }
  free(buff);
  return (0);
}

int             calc_width_height(t_astar *a, char **av)
{
  FILE          *fd;
  char          *buff;
  int           check;
  size_t        size;

  size = 0;
  fd = NULL;
  buff = NULL;
  if ((fd = fopen(av[1], "r+")) == NULL)
    my_perror("fopen");
  check = calc_width_height2(buff, fd, a, size);
  fclose(fd);
  return (check);
}

void		no_sep(t_astar *a, char *buff, int i)
{
  int		j;

  j = 0;
  while (buff[j] != '\n' && buff[j] != '\0')
    {
      a->tab[i][j] = buff[j];
      j++;
    }
  a->tab[i][j] = '\0';
}

void		load_file(t_astar *a, char **av)
{
  FILE*         fd;
  char          *buff;
  size_t        size;
  int		i;

  i = 0;
  buff = NULL;
  if ((fd = fopen(av[1], "r+")) == NULL)
    my_perror("fopen");
  if ((a->tab = malloc(sizeof(char *) * (a->height + 1))) == NULL)
    my_perror("malloc");
  while ((getline(&buff, &size, fd)) != -1)
    {
      if ((a->tab[i] = malloc(sizeof(char) * (strlen(buff) + 1))) == NULL)
	my_perror("malloc");
      no_sep(a, buff, i);
      i++;
    }
  a->tab[i] = NULL;
  free(buff);
  fclose(fd);
}
