/*
** distance.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Sat Apr 30 16:26:11 2016 Vincent Le Quec
** Last update Wed May 18 15:41:04 2016 Vincent Le Quec
*/

#include "solver.h"

double		my_pow(int nb, int power)
{
  if (power < 0)
    return (0);
  if (power == 0)
    return (1);
  return (nb * my_pow(nb, power - 1));
}

double          my_sqrt(int nb)
{
  double        res;
  int           i;

  res = 2.0;
  i = PRECISION;
  if (nb < 0)
    res = -1.0;
  else
    while (i >= 0)
      {
	res = ((nb - res * res) / (2 * res)) + res;
	i--;
      }
  return (res);
}

double          distance(t_astar *a, int x, int y)
{
  return (my_sqrt(my_pow(a->end.x - x, 2) + my_pow(a->end.y - y, 2)));
}
