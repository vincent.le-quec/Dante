/*
** display.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Thu May 12 17:44:28 2016 Vincent Le Quec
** Last update Thu May 12 17:47:07 2016 Vincent Le Quec
*/

#include "solver.h"

void            display_tab(char **tab)
{
  int           i;

  i = 0;
  while (tab[i])
    printf("%s\n", tab[i++]);
}

void            display_tab_color(char **tab)
{
  int           i;
  int           j;

  i = 0;
  while (tab[i])
    {
      j = 0;
      while (tab[i][j])
        {
          if (tab[i][j] == 'o')
            printf(BOLDGREEN "o" RESET);
          else
            if (tab[i][j] == 'X')
              printf(MAGENTA "X" RESET);
	    else
	      printf(BOLDBLUE "*" RESET);
          j++;
        }
      printf("\n");
      i++;
    }
}
