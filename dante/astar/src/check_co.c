/*
** check_co.c for  in /home/le-que_v/rendu/dante/astar
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Sat Apr 30 19:09:37 2016 Vincent Le Quec
** Last update Fri May 20 17:03:42 2016 remy PELLETIER
*/

#include "solver.h"

int		nb_co(double *res)
{
  int		i;
  int		nb;

  i = 0;
  nb = 0;
  while (i < 4)
    {
      if (res[i] != -1.0)
	nb++;
      i++;
    }
  return (nb);
}

int		check_co_back(t_astar *a, char c)
{
  if (a->cell.x != a->width && a->tab[a->cell.y][a->cell.x + 1] == c)
      return (1);
  if (a->cell.x != 0 && a->tab[a->cell.y][a->cell.x - 1] == c)
      return (1);
  if (a->cell.y != a->height - 1 && a->tab[a->cell.y + 1][a->cell.x] == c)
      return (1);
  if (a->cell.y != 0 && a->tab[a->cell.y - 1][a->cell.x] == c)
      return (1);
  return (0);
}

int             check_co(t_astar *a, char c)
{
  double        res[4];

  res[0] = check_right(a, c);
  res[1] = check_left(a, c);
  res[2] = check_bottom(a, c);
  res[3] = check_top(a, c);
  if (nb_co(res) > 1)
    {
      a->tmp.x = a->cell.x;
      a->tmp.y = a->cell.y;
    }
  if (check_min(a, res) == 1)
    return (1);
  return (0);
}
