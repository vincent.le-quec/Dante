##
## Makefile for  in /home/le-que_v/rendu/dante
## 
## Made by Vincent Le Quec
## Login   <le-que_v@epitech.net>
## 
## Started on  Tue May 10 11:29:17 2016 Vincent Le Quec
## Last update Fri May 27 16:40:09 2016 Vincent Le Quec
##

# All
CC		=	gcc -W -Wall -Werror -ansi -pedantic

RM		=	rm -f

CFLAGS		=	$(INC_ASTAR) $(INC_PROFONDEUR) $(INC_LARGEUR) $(INC_TOURNOI) $(INC_GENERATEUR)

# Astar
ASTAR		=	./astar/solver

SRCS_ASTAR	=	./astar/src/main.c			\
			./astar/src/create_tab.c              	\
			./astar/src/algo.c                    	\
			./astar/src/check_co.c                	\
			./astar/src/check_co2.c                	\
			./astar/src/tools/my_str_to_tab.c     	\
			./astar/src/tools/distance.c          	\
			./astar/src/tools/tools.c             	\
			./astar/src/tools/error.c		\
			./astar/src/tools/display.c

OBJS_ASTAR	=	$(SRCS_ASTAR:.c=.o)

INC_ASTAR	=	-I./astar/include/


# Largeur
LARGEUR		=	./largeur/solver

SRCS_LARGEUR	=	./largeur/src/main.c			\
			./largeur/src/check_position.c		\
			./largeur/src/find_position.c		\
			./largeur/src/create_tab.c		\
			./largeur/src/algo.c			\
			./largeur/src/tools/init_struct.c	\
			./largeur/src/tools/display.c		\
			./largeur/src/tools/tools.c

OBJS_LARGEUR	=	$(SRCS_LARGEUR:.c=.o)

INC_LARGEUR	=	-I./largeur/include/


# Profondeur
PROFONDEUR	=	./profondeur/solver

SRCS_PROFONDEUR	=	./profondeur/src/main.c            	\
			./profondeur/src/check.c           	\
			./profondeur/src/tab.c             	\
			./profondeur/src/algo.c             	\
			./profondeur/src/init_struct.c         	\
			./profondeur/src/tools/error.c     	\
			./profondeur/src/tools/tools.c

OBJS_PROFONDEUR	=	$(SRCS_PROFONDEUR:.c=.o)

INC_PROFONDEUR	=	-I./profondeur/include/


# Tournoi
TOURNOI		=	./tournoi/solver

SRCS_TOURNOI	=	./tournoi/src/main.c			\
			./tournoi/src/create_tab.c              \
			./tournoi/src/algo.c                    \
			./tournoi/src/check_co.c                \
			./tournoi/src/check_co2.c               \
			./tournoi/src/tools/my_str_to_tab.c     \
			./tournoi/src/tools/distance.c          \
			./tournoi/src/tools/tools.c             \
			./tournoi/src/tools/error.c		\
			./tournoi/src/tools/display.c

OBJS_TOURNOI	=	$(SRCS_TOURNOI:.c=.o)

INC_TOURNOI	=	-I./tournoi/include/


# Generateur
GENERATEUR	=	./generateur/generateur

SRCS_GENERATEUR	=	./generateur/src/main.c			\
			./generateur/src/check_num.c		\
			./generateur/src/check_position.c	\
			./generateur/src/algo.c			\
			./generateur/src/tab.c			\
			./generateur/src/imperfect/imperfect.c	\
			./generateur/src/perfect/perfect.c	\
			./generateur/src/tools/tools.c		\
			./generateur/src/tools/my_getnbr.c	\

OBJS_GENERATEUR	=	$(SRCS_GENERATEUR:.c=.o)

INC_GENERATEUR	=	-I./generateur/include/

all		:	$(ASTAR) $(PROFONDEUR) $(LARGEUR) $(TOURNOI) $(GENERATEUR)
astar		:	$(ASTAR)
largeur		:	$(LARGEUR)
profondeur	:	$(PROFONDEUR)
tournoi		:	$(TOURNOI)
generateur	:	$(GENERATEUR)

$(ASTAR)	:	$(OBJS_ASTAR)
		$(CC)	$(OBJS_ASTAR) 	   -o $(ASTAR)		$(INC_ASTAR)
$(LARGEUR)	:	$(OBJS_LARGEUR)
		$(CC)	$(OBJS_LARGEUR)    -o $(LARGEUR)	$(INC_LARGEUR)
$(PROFONDEUR)	:	$(OBJS_PROFONDEUR)
		$(CC) 	$(OBJS_PROFONDEUR) -o $(PROFONDEUR)	$(INC_PROFONDEUR)
$(TOURNOI)	:	$(OBJS_TOURNOI)
		$(CC) 	$(OBJS_TOURNOI)	   -o $(TOURNOI) 	$(INC_TOURNOI)
$(GENERATEUR)	:	$(OBJS_GENERATEUR)
		$(CC) 	$(OBJS_GENERATEUR) -o $(GENERATEUR) 	$(INC_GENERATEUR)

clean		:
		$(RM) $(OBJS_ASTAR) $(OBJS_LARGEUR) $(OBJS_PROFONDEUR) $(OBJS_TOURNOI) $(OBJS_GENERATEUR)
clean_astar	:
		$(RM) $(OBJS_ASTAR)
clean_largeur	:
		$(RM) $(OBJS_LARGEUR)
clean_profondeur:
		$(RM) $(OBJS_PROFONDEUR)
clean_tournoi	:
		$(RM) $(OBJS_TOURNOI)
clean_generateur:
		$(RM) $(OBJS_GENERATEUR)

fclean		:	clean
		$(RM) $(ASTAR) $(LARGEUR) $(PROFONDEUR) $(TOURNOI) $(GENERATEUR)
fclean_astar	:	clean_astar
		$(RM) $(ASTAR)
fclean_largeur	:	clean_largeur
		$(RM) $(LARGEUR)
fclean_profondeur:	clean_profondeur
		$(RM) $(PROFONDEUR)
fclean_tournoi	:	clean_tournoi
		$(RM) $(TOURNOI)
fclean_generateur:	clean_generateur
		$(RM) $(GENERATEUR)

re		:	fclean all
