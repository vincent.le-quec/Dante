/*
** check_num.c for check_num in /home/pellet_r/CPE/dante/generateur
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Wed May  4 12:59:14 2016 remy PELLETIER
** Last update Fri May 27 16:56:23 2016 Vincent Le Quec
*/

#include "generateur.h"

int             find_nb_true(int *nb_choice)
{
  int           i;
  int           nb_true;

  i = 0;
  nb_true = 0;
  while (i < 4)
    if (nb_choice[i++] != 0)
      nb_true++;
  return (nb_true);
}

void            swap_tab(int nb_true, int *nb_choice, t_labyrinth *lab)
{
  int           i;
  int           k;

  nb_true = nb_true;
  k = 0;
  i = 0;
  while (i < 4)
    {
      if (nb_choice[i] != 0)
	{
          lab->res_tab[k] = nb_choice[i];
          k++;
        }
      i++;
    }
}

int             what_num(t_labyrinth *lab, int i, int j, char c)
{
  int           nb_alea;
  int           nb_choice[4];
  int           nb_true;
  int           res;

  nb_choice[0] = check_left(lab, i, j, c);
  nb_choice[1] = check_right(lab, i, j, c);
  nb_choice[2] = check_top(lab, i, j, c);
  nb_choice[3] = check_bottom(lab, i, j, c);
  if ((nb_true = find_nb_true(nb_choice)) == 0)
    return (0);
  swap_tab(nb_true, nb_choice, lab);
  nb_alea = rand() % nb_true;
  res = lab->res_tab[nb_alea];
  return (res);
}
