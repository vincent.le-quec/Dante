/*
** tab.c for tab in /home/pellet_r/CPE/dante/generateur
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Wed May  4 12:54:20 2016 remy PELLETIER
** Last update Fri May 27 16:57:00 2016 Vincent Le Quec
*/

#include "generateur.h"

int             create_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  if ((lab->tab = malloc(sizeof(char *) * lab->height)) == NULL)
    my_perror("malloc");
  while (i < lab->height)
    {
      if ((lab->tab[i] = malloc(sizeof(char) * lab->width)) == NULL)
	my_perror("malloc");
      i++;
    }
  return (0);
}

void            add_grille(t_labyrinth *lab)
{
  int           i;
  int           j;

  i = 1;
  while (i < lab->height)
    {
      j = 1;
      while (j < lab->width)
        {
          if (i % 2 == 0)
            lab->tab[i - 1][j - 1] = 'X';
          else if (j % 2 == 0)
            lab->tab[i - 1][j - 1] = 'X';
          else
            lab->tab[i - 1][j - 1] = '0';
          j++;
        }
      lab->tab[i - 1][j - 1] = '\0';
      i++;
    }
  free(lab->tab[i - 1]);
  lab->tab[i - 1] = NULL;
}
