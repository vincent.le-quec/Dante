/*
** my_getnbr.c for my_getnbr in /home/pellet_r
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Mon Oct 12 14:20:52 2015 remy PELLETIER
** Last update Fri May 27 16:57:09 2016 Vincent Le Quec
*/

#include "generateur.h"

int	check_error(char *str)
{
  while (*str)
    {
      if ((*str) > '9' || (*str) < '0')
	return (-1);
      str++;
    }
  return (0);
}

int     my_getnbr(char *str)
{
  if ((check_error(str)) == -1)
    return (-1);
  return (atoi(str));
}
