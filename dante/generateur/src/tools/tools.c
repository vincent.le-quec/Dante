/*
** tools.c for  in /home/le-que_v/rendu/dante/generateur
** 
** Made by Vincent Le Quec
** Login   <le-que_v@epitech.net>
** 
** Started on  Mon May  2 16:19:44 2016 Vincent Le Quec
** Last update Sat May 28 16:00:16 2016 Vincent Le Quec
*/

#include "generateur.h"

void            display_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  while (lab->tab[i])
    printf("%s\n", lab->tab[i++]);
}

int             init_struct(t_labyrinth *lab, char **av)
{
  lab->tab = NULL;
  if ((lab->width = my_getnbr(av[1])) == -1 ||
      (lab->height = my_getnbr(av[2])) == -1)
    return (-1);
  if (lab->width <= 0 || lab->height <= 0)
    return (-1);
  lab->width = lab->width + 1;
  lab->height = lab->height + 1;
  return (0);
}

void            free_tab(t_labyrinth *lab)
{
  int           i;

  i = 0;
  while (lab->tab[i])
    free(lab->tab[i++]);
  free(lab->tab);
}

void            epur_tab(t_labyrinth *lab)
{
  int           i;
  int           j;

  j = 0;
  i = 0;
  while (lab->tab[i])
    {
      j = 0;
      while (lab->tab[i][j])
        {
          if (lab->tab[i][j] == 'A')
            lab->tab[i][j] = '*';
          j++;
        }
      i++;
    }
}

void            my_perror(char *str)
{
  perror(str);
  exit(1);
}
