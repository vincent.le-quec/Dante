/*
** algo.c for algo in /home/pellet_r/CPE/dante/generateur
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Wed May  4 13:02:25 2016 remy PELLETIER
** Last update Fri May 27 16:54:42 2016 Vincent Le Quec
*/

#include "generateur.h"

int		what_position2(t_labyrinth *lab, int pos, int nb_alea, char c)
{
  if (nb_alea == 3)
    {
      if (c == '0')
	{
	  lab->tab[lab->i + 1][lab->j] = '*';
	  return (pos + ((lab->width * 2)));
	}
      else
	return (pos + lab->width);
    }
  if (nb_alea == 4)
    {
      if (c == '0')
	{
	  lab->tab[lab->i][lab->j - 1] = '*';
	  return (pos - 2);
	}
      else
	return (pos - 1);
    }
  return (0);
}

int		what_position(t_labyrinth *lab, int pos, int nb_alea, char c)
{
  if (nb_alea == 1)
    {
      if (c == '0')
	{
	  lab->tab[lab->i - 1][lab->j] = '*';
	  return (pos - ((lab->width * 2)));
	}
      else
	return (pos - lab->width);
    }
  if (nb_alea == 2)
    {
      if (c == '0')
	{
	  lab->tab[lab->i][lab->j + 1] = '*';
	  return (pos + 2);
	}
      else
	return (pos + 1);
    }
  return (what_position2(lab, pos, nb_alea, c));
}

int		algo(t_labyrinth *lab, int pos, int nb_case)
{
  int		nb_alea;

  while (nb_case != lab->end + 1)
    {
      lab->i = pos / (lab->width);
      lab->j = pos % (lab->width);
      if ((nb_alea = what_num(lab, lab->i, lab->j, '0')) != 0)
	{
	  lab->tab[lab->i][lab->j] = '*';
	  pos = what_position(lab, pos, nb_alea, '0');
	  nb_case++;
	}
      else
	{
	  lab->tab[lab->i][lab->j] = 'A';
	  nb_alea = what_num(lab, lab->i, lab->j, '*');
	  pos = what_position(lab, pos, nb_alea,  '*');
	}
      if (lab->i == 0 && lab->j == 0)
	nb_case++;
    }
  lab->i = pos / (lab->width);
  lab->j = pos % (lab->width);
  lab->tab[lab->i][lab->j] = '*';
  return (0);
}

int		calc_width_height(t_labyrinth *lab)
{
  int		i;
  int		height;
  int		width;

  width = 0;
  height = 0;
  i = 0;
  while (lab->tab[i])
    {
      if (lab->tab[i][0] == '0')
	height++;
      i++;
    }
  i = 0;
  while (lab->tab[0][i])
    {
      if (lab->tab[0][i] == '0')
	width++;
      i++;
    }
  lab->end = width * height;
  return (0);
}

int		labyrinth(t_labyrinth *lab)
{
  if ((create_tab(lab)) == -1)
    return (-1);
  add_grille(lab);
  calc_width_height(lab);
  algo(lab, 0, 1);
  return (0);
}

