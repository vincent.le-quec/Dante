/*
** main.c for main in 
** 
** Made by remy PELLETIER
** Login   <pellet_r@epitech.net>
** 
** Started on  Mon Apr 25 10:53:09 2016 remy PELLETIER
** Last update Fri May 27 16:56:37 2016 Vincent Le Quec
*/

#include "generateur.h"

void            broken_mur(t_labyrinth *lab)
{
  int           i;
  int           j;

  i = 0;
  while (lab->tab[i])
    {
      j = 0;
      while (lab->tab[i][j])
	{
          if ((j != lab->width  && i != lab->height) &&
	      (lab->tab[i][j] == 'X' && j % 3 == 0))
	    lab->tab[i][j] = '*';
          j++;
	}
      i++;
    }
}

int		imperfect(t_labyrinth *lab)
{
  labyrinth(lab);
  epur_tab(lab);
  broken_mur(lab);
  make_end(lab);
  display_tab(lab);
  free_tab(lab);
  free(lab);
  return (0);
}
